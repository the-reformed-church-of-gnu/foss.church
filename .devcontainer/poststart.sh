#!/bin/bash

set -eux

echo "Set git-hook path"
cd /foss
git config core.hooksPath '.git-hooks'
npm install --global git-conventional-commits

echo "Install successfull"
